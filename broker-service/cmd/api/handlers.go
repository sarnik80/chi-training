package main

import (
	"encoding/json"
	"net/http"
)

type jsonResponse struct {
	Error   bool   `json:"error"`
	Message string `json:"message"`
	Data    any    `json:"data,omitempty"`
}

// first of all this is a handler

/* it will rceive some kind of payload => we ignore that this moment
   we are going to sending JSON as our response
*/
func (app *Config) Broker(w http.ResponseWriter, r *http.Request) {

	// we are creating a json message
	payload := jsonResponse{
		Error:   false,
		Message: "hit the broker",
	}

	out, _ := json.MarshalIndent(payload, "", "\t")

	// set the header
	w.Header().Set("Content-Type", "application/json")

	// write the header
	w.WriteHeader(http.StatusAccepted)

	w.Write([]byte(out))
}
