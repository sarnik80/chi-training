package main

import (
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/cors"
)

// this file will be all of the routes for our application

// we must add a receiver for this function
// to share any configuration we might have from our application with routes in case we need them

func (app *Config) routes() http.Handler {

	// mux is the common name when you're using routing
	mux := chi.NewRouter()

	// specify who is allowed to connect
	mux.Use(cors.Handler(cors.Options{

		// Use this to allow specific origin hosts
		// AllowedOrigins:   []string{"https://foo.com"},
		AllowedOrigins:   []string{"https://*", "http://*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: true,
		MaxAge:           300,
	}))

	/*     this is a bult into chi   called  middleware.Heartbeat()
	       we set heartbeat URL to /ping
		   This will allow us easily make sure that this service is still alive
	*/
	mux.Use(middleware.Heartbeat("/ping"))

	mux.Post("/", app.Broker)

	return mux
}
